package org.gitnex.tea4j.v2.apis;

import java.util.Date;
import java.util.List;
import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.NotificationCount;
import org.gitnex.tea4j.v2.models.NotificationThread;
import retrofit2.Call;
import retrofit2.http.*;

public interface NotificationApi {
  /**
   * List users&#x27;s notification threads
   *
   * @param all If true, show notifications marked as read. Default value is false (optional)
   * @param statusTypes Show notifications with the provided status types. Options are: unread, read
   *     and/or pinned. Defaults to unread &amp; pinned. (optional)
   * @param subjectType filter notifications by subject type (optional)
   * @param since Only show notifications updated after the given time. This is a timestamp in RFC
   *     3339 format (optional)
   * @param before Only show notifications updated before the given time. This is a timestamp in RFC
   *     3339 format (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;NotificationThread&gt;&gt;
   */
  @GET("notifications")
  Call<List<NotificationThread>> notifyGetList(
      @retrofit2.http.Query("all") Boolean all,
      @retrofit2.http.Query("status-types") List<String> statusTypes,
      @retrofit2.http.Query("subject-type") List<String> subjectType,
      @retrofit2.http.Query("since") Date since,
      @retrofit2.http.Query("before") Date before,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * List users&#x27;s notification threads on a specific repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param all If true, show notifications marked as read. Default value is false (optional)
   * @param statusTypes Show notifications with the provided status types. Options are: unread, read
   *     and/or pinned. Defaults to unread &amp; pinned (optional)
   * @param subjectType filter notifications by subject type (optional)
   * @param since Only show notifications updated after the given time. This is a timestamp in RFC
   *     3339 format (optional)
   * @param before Only show notifications updated before the given time. This is a timestamp in RFC
   *     3339 format (optional)
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;NotificationThread&gt;&gt;
   */
  @GET("repos/{owner}/{repo}/notifications")
  Call<List<NotificationThread>> notifyGetRepoList(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("all") Boolean all,
      @retrofit2.http.Query("status-types") List<String> statusTypes,
      @retrofit2.http.Query("subject-type") List<String> subjectType,
      @retrofit2.http.Query("since") Date since,
      @retrofit2.http.Query("before") Date before,
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit);

  /**
   * Get notification thread by ID
   *
   * @param id id of notification thread (required)
   * @return Call&lt;NotificationThread&gt;
   */
  @GET("notifications/threads/{id}")
  Call<NotificationThread> notifyGetThread(@retrofit2.http.Path("id") String id);

  /**
   * Check if unread notifications exist
   *
   * @return Call&lt;NotificationCount&gt;
   */
  @GET("notifications/new")
  Call<NotificationCount> notifyNewAvailable();

  /**
   * Mark notification threads as read, pinned or unread
   *
   * @param lastReadAt Describes the last point that notifications were checked. Anything updated
   *     since this time will not be updated. (optional)
   * @param all If true, mark all notifications on this repo. Default value is false (optional)
   * @param statusTypes Mark notifications with the provided status types. Options are: unread, read
   *     and/or pinned. Defaults to unread. (optional)
   * @param toStatus Status to mark notifications as, Defaults to read. (optional)
   * @return Call&lt;List&lt;NotificationThread&gt;&gt;
   */
  @PUT("notifications")
  Call<List<NotificationThread>> notifyReadList(
      @retrofit2.http.Query("last_read_at") Date lastReadAt,
      @retrofit2.http.Query("all") String all,
      @retrofit2.http.Query("status-types") List<String> statusTypes,
      @retrofit2.http.Query("to-status") String toStatus);

  /**
   * Mark notification threads as read, pinned or unread on a specific repo
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @param all If true, mark all notifications on this repo. Default value is false (optional)
   * @param statusTypes Mark notifications with the provided status types. Options are: unread, read
   *     and/or pinned. Defaults to unread. (optional)
   * @param toStatus Status to mark notifications as. Defaults to read. (optional)
   * @param lastReadAt Describes the last point that notifications were checked. Anything updated
   *     since this time will not be updated. (optional)
   * @return Call&lt;List&lt;NotificationThread&gt;&gt;
   */
  @PUT("repos/{owner}/{repo}/notifications")
  Call<List<NotificationThread>> notifyReadRepoList(
      @retrofit2.http.Path("owner") String owner,
      @retrofit2.http.Path("repo") String repo,
      @retrofit2.http.Query("all") String all,
      @retrofit2.http.Query("status-types") List<String> statusTypes,
      @retrofit2.http.Query("to-status") String toStatus,
      @retrofit2.http.Query("last_read_at") Date lastReadAt);

  /**
   * Mark notification thread as read by ID
   *
   * @param id id of notification thread (required)
   * @param toStatus Status to mark notifications as (optional, default to read)
   * @return Call&lt;NotificationThread&gt;
   */
  @PATCH("notifications/threads/{id}")
  Call<NotificationThread> notifyReadThread(
      @retrofit2.http.Path("id") String id, @retrofit2.http.Query("to-status") String toStatus);
}
