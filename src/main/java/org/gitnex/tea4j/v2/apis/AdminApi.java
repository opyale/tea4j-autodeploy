package org.gitnex.tea4j.v2.apis;

import java.util.List;
import org.gitnex.tea4j.v2.CollectionFormats.*;
import org.gitnex.tea4j.v2.models.CreateKeyOption;
import org.gitnex.tea4j.v2.models.CreateOrgOption;
import org.gitnex.tea4j.v2.models.CreateRepoOption;
import org.gitnex.tea4j.v2.models.CreateUserOption;
import org.gitnex.tea4j.v2.models.Cron;
import org.gitnex.tea4j.v2.models.EditUserOption;
import org.gitnex.tea4j.v2.models.Organization;
import org.gitnex.tea4j.v2.models.PublicKey;
import org.gitnex.tea4j.v2.models.Repository;
import org.gitnex.tea4j.v2.models.User;
import retrofit2.Call;
import retrofit2.http.*;

public interface AdminApi {
  /**
   * Adopt unadopted files as a repository
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Void&gt;
   */
  @POST("admin/unadopted/{owner}/{repo}")
  Call<Void> adminAdoptRepository(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Create an organization
   *
   * @param body (required)
   * @param username username of the user that will own the created organization (required)
   * @return Call&lt;Organization&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users/{username}/orgs")
  Call<Organization> adminCreateOrg(
      @retrofit2.http.Body CreateOrgOption body, @retrofit2.http.Path("username") String username);

  /**
   * Add a public key on behalf of a user
   *
   * @param username username of the user (required)
   * @param body (optional)
   * @return Call&lt;PublicKey&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users/{username}/keys")
  Call<PublicKey> adminCreatePublicKey(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Body CreateKeyOption body);

  /**
   * Create a repository on behalf of a user
   *
   * @param body (required)
   * @param username username of the user. This user will own the created repository (required)
   * @return Call&lt;Repository&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users/{username}/repos")
  Call<Repository> adminCreateRepo(
      @retrofit2.http.Body CreateRepoOption body, @retrofit2.http.Path("username") String username);

  /**
   * Create a user
   *
   * @param body (optional)
   * @return Call&lt;User&gt;
   */
  @Headers({"Content-Type:application/json"})
  @POST("admin/users")
  Call<User> adminCreateUser(@retrofit2.http.Body CreateUserOption body);

  /**
   * List cron tasks
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Cron&gt;&gt;
   */
  @GET("admin/cron")
  Call<List<Cron>> adminCronList(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * Run cron task
   *
   * @param task task to run (required)
   * @return Call&lt;Void&gt;
   */
  @POST("admin/cron/{task}")
  Call<Void> adminCronRun(@retrofit2.http.Path("task") String task);

  /**
   * Delete unadopted files
   *
   * @param owner owner of the repo (required)
   * @param repo name of the repo (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("admin/unadopted/{owner}/{repo}")
  Call<Void> adminDeleteUnadoptedRepository(
      @retrofit2.http.Path("owner") String owner, @retrofit2.http.Path("repo") String repo);

  /**
   * Delete a user
   *
   * @param username username of user to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("admin/users/{username}")
  Call<Void> adminDeleteUser(@retrofit2.http.Path("username") String username);

  /**
   * Delete a user&#x27;s public key
   *
   * @param username username of user (required)
   * @param id id of the key to delete (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("admin/users/{username}/keys/{id}")
  Call<Void> adminDeleteUserPublicKey(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Path("id") Long id);

  /**
   * Edit an existing user
   *
   * @param username username of user to edit (required)
   * @param body (optional)
   * @return Call&lt;User&gt;
   */
  @Headers({"Content-Type:application/json"})
  @PATCH("admin/users/{username}")
  Call<User> adminEditUser(
      @retrofit2.http.Path("username") String username, @retrofit2.http.Body EditUserOption body);

  /**
   * List all organizations
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;Organization&gt;&gt;
   */
  @GET("admin/orgs")
  Call<List<Organization>> adminGetAllOrgs(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * List all users
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @return Call&lt;List&lt;User&gt;&gt;
   */
  @GET("admin/users")
  Call<List<User>> adminGetAllUsers(
      @retrofit2.http.Query("page") Integer page, @retrofit2.http.Query("limit") Integer limit);

  /**
   * List unadopted repositories
   *
   * @param page page number of results to return (1-based) (optional)
   * @param limit page size of results (optional)
   * @param pattern pattern of repositories to search for (optional)
   * @return Call&lt;List&lt;String&gt;&gt;
   */
  @GET("admin/unadopted")
  Call<List<String>> adminUnadoptedList(
      @retrofit2.http.Query("page") Integer page,
      @retrofit2.http.Query("limit") Integer limit,
      @retrofit2.http.Query("pattern") String pattern);
}
