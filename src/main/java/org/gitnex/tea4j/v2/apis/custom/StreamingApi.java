package org.gitnex.tea4j.v2.apis.custom;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

public interface StreamingApi {

  @Streaming
  @GET("repos/{owner}/{repo}/pulls/{index}.diff")
  Call<ResponseBody> getPullDiffContent(
      @Path("owner") String owner, @Path("repo") String repo, @Path("index") String index);
}
