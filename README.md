![Build status](https://ci.codeberg.org/api/badges/opyale/tea4j-autodeploy/status.svg?branch=main)
![Java version](https://img.shields.io/badge/java-8+-informational)
![License](https://img.shields.io/badge/license-GPLv3-green)

<p align="center">
    <img src="assets/tea4j-autodeploy-logo.png" alt="Logo" width="250px">
</p>

# Tea4j-autodeploy

Code is automatically generated from the official gitea swagger template using the Codeberg CI.

## Installation

Adding this dependency to your project:

<table>
<tr>
<th>Gradle</th>
<th>Maven</th>
</tr>
<tr>
<td>

```groovy
allprojects {
    repositories {
        maven { url 'https://jitpack.io' }
    }
}

dependencies {
    implementation 'org.codeberg.opyale:tea4j-autodeploy:{commit-sha}'
}
```

</td>
<td>

```xml
<repositories>
    <repository>
        <id>jitpack.io</id>
        <url>https://jitpack.io</url>
    </repository>
</repositories>

<dependencies>
    <dependency>
        <groupId>org.codeberg.opyale</groupId>
        <artifactId>tea4j-autodeploy</artifactId>
        <version>{commit-sha}</version>
    </dependency>
</dependencies>
```

</td>
</tr>
</table>