# EditDeadlineOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**dueDate** | [**Date**](Date.md) |  | 
