# Cron

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**execTimes** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**next** | [**Date**](Date.md) |  |  [optional]
**prev** | [**Date**](Date.md) |  |  [optional]
**schedule** | **String** |  |  [optional]
