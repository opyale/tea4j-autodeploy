# Milestone

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**closedAt** | [**Date**](Date.md) |  |  [optional]
**closedIssues** | **Long** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**description** | **String** |  |  [optional]
**dueOn** | [**Date**](Date.md) |  |  [optional]
**id** | **Long** |  |  [optional]
**openIssues** | **Long** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
