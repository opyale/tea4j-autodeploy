# PullRequestMeta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**merged** | **Boolean** |  |  [optional]
**mergedAt** | [**Date**](Date.md) |  |  [optional]
