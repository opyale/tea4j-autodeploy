# Commit

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | [**User**](User.md) |  |  [optional]
**commit** | [**RepoCommit**](RepoCommit.md) |  |  [optional]
**committer** | [**User**](User.md) |  |  [optional]
**created** | [**Date**](Date.md) |  |  [optional]
**files** | [**List&lt;CommitAffectedFiles&gt;**](CommitAffectedFiles.md) |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**parents** | [**List&lt;CommitMeta&gt;**](CommitMeta.md) |  |  [optional]
**sha** | **String** |  |  [optional]
**url** | **String** |  |  [optional]
