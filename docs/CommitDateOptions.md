# CommitDateOptions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**author** | [**Date**](Date.md) |  |  [optional]
**committer** | [**Date**](Date.md) |  |  [optional]
