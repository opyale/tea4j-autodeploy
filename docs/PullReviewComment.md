# PullReviewComment

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**body** | **String** |  |  [optional]
**commitId** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**diffHunk** | **String** |  |  [optional]
**htmlUrl** | **String** |  |  [optional]
**id** | **Long** |  |  [optional]
**originalCommitId** | **String** |  |  [optional]
**originalPosition** | **Integer** |  |  [optional]
**path** | **String** |  |  [optional]
**position** | **Integer** |  |  [optional]
**pullRequestReviewId** | **Long** |  |  [optional]
**pullRequestUrl** | **String** |  |  [optional]
**resolver** | [**User**](User.md) |  |  [optional]
**updatedAt** | [**Date**](Date.md) |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
