# CreateMilestoneOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  |  [optional]
**dueOn** | [**Date**](Date.md) |  |  [optional]
**state** | [**StateEnum**](#StateEnum) |  |  [optional]
**title** | **String** |  |  [optional]

<a name="StateEnum"></a>
## Enum: StateEnum
Name | Value
---- | -----
OPEN | &quot;open&quot;
CLOSED | &quot;closed&quot;
