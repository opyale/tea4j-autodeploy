# EditIssueOption

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**assignee** | **String** | deprecated |  [optional]
**assignees** | **List&lt;String&gt;** |  |  [optional]
**body** | **String** |  |  [optional]
**dueDate** | [**Date**](Date.md) |  |  [optional]
**milestone** | **Long** |  |  [optional]
**ref** | **String** |  |  [optional]
**state** | **String** |  |  [optional]
**title** | **String** |  |  [optional]
**unsetDueDate** | **Boolean** |  |  [optional]
