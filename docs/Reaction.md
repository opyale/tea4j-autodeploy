# Reaction

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **String** |  |  [optional]
**createdAt** | [**Date**](Date.md) |  |  [optional]
**user** | [**User**](User.md) |  |  [optional]
